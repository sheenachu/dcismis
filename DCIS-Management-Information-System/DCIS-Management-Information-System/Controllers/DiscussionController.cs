﻿using DCIS_Management_Information_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCIS_Management_Information_System.Controllers
{
    public class DiscussionController : Controller
    {
        // GET: Discussion
        public ActionResult Index()
        {
            return View();
        }
        /*Eldrin Controller*/
        public ActionResult Topics(int offered_course_id)
        {

            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Topic gd = new Topic();
            Students_in_offered_courses sad = new Students_in_offered_courses();
            var pendingUser = dcis.Users.SqlQuery("SELECT * FROM [User] u Join Students_in_offered_courses s ON u.schoolId = s.school_id WHERE u.userType = 'student' AND s.request_status = 'pending' AND s.offered_id =" + offered_course_id);
            var acceptedUserStudent = dcis.Users.SqlQuery("SELECT * FROM [User] u Join Students_in_offered_courses s ON u.schoolId = s.school_id WHERE u.userType = 'student' AND s.request_status = 'accepted'  AND s.offered_id =" + offered_course_id);
            var invitedTeacher = dcis.Users.SqlQuery("SELECT * FROM  [User] u JOIN Teacher t ON u.schoolId = t.school_id WHERE t.offered_id = " + offered_course_id +" AND (u.userType = 'Teacher' OR u.userType = 'Chairmain')");

            var us = (from u in dcis.Topics select u).Where( m => m.offered_id == offered_course_id);

            ViewBag.Offered_Id = offered_course_id;

            ViewData["ListOfTopics"] = us.ToList();

            ViewData["pendingUser"] = pendingUser.ToList();
            ViewData["acceptedStudent"] = acceptedUserStudent.ToList();
            ViewData["invitedTeacher"] = invitedTeacher.ToList();
            return View();
        }
        /*
        public ActionResult ArchivedTopics(FormCollection fc)
        {

            DCISMISEntities3 dcis = new DCISMISEntities3();
            Offered_Course_Discussion gd = new Offered_Course_Discussion();

            var us = (from u in dcis.Offered_Course_Discussion select u).Where(status => status.status== 0 );
            ViewData["ListOfArchivedTopics"] = us.ToList();
            Response.Write("<script>alert('Data inserted successfully')</script>");
            return View("Topics");
        }
        */
        [HttpPost]
        public ActionResult UpdateTopic(FormCollection fc)
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();

            int discussionID = Convert.ToInt32(fc["offered_id"]);
            int topicID = Convert.ToInt32(fc["offered_course_discussion_id"]);
            string title = fc["discussion_title"].ToString();
            

            var d = dcis.Topics.SingleOrDefault(b => b.topic_id == topicID);

            d.topic_id = topicID;
            d.status = 1;
            d.discussion_title = title;

            try
            {
                dcis.SaveChanges();
                ViewBag.Result = "Saved";
            }
            catch (Exception e)
            {
                ViewBag.Result = "Not Saved";
            }

            return RedirectToAction("Topics", new { offered_course_id = discussionID });
        }

        public ActionResult ArchiveDetails(int id, int offered_id)
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Discussion d = new Discussion();


            var res = dcis.Topics.SingleOrDefault(b => b.topic_id == id);
            res.status = 0;

            try
            {
                dcis.SaveChanges();
                ViewBag.Result = "Saved";
            }
            catch (Exception e)
            {
                throw e;
            }

            return RedirectToAction("Topics", new { offered_course_id = offered_id });

        }
        public ActionResult RestoreArchiveDetails(int id, int offered_id)
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Discussion d = new Discussion();


            var res = dcis.Topics.SingleOrDefault(b => b.topic_id == id);
            res.status = 1;

            try
            {
                dcis.SaveChanges();
                ViewBag.Result = "Saved";
            }
            catch (Exception e)
            {
                throw e;
            }

            return RedirectToAction("Topics", new { offered_course_id = offered_id });

        }
        public ActionResult GetEditDetails(int id)
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Discussion d = new Discussion();


            var res = dcis.Topics.Find(id);
            d.offered_course_discussion_id = res.topic_id;
            d.offered_id = res.offered_id;
            d.user_id = res.user_id;
            d.datetime = res.datetime;
            d.discussion_title = res.discussion_title;
            d.status = res.status;

            return PartialView("EditDetail", d);

        }
        [HttpPost]
        public ActionResult GetEditDetails(Discussion d)
        {

            return PartialView("EditDetail");

        }
        public ActionResult ArchiveTopic(int id)
        {
            return View();
        }
        public ActionResult AddTopic(FormCollection fc)
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Topic gd = new Topic();
            DateTime now = DateTime.Now;
            String title = fc["TopicTitle"].ToString();
            int offered_id = Convert.ToInt32(fc["offeredID"]);
            int user_id = 2;
            int status = 1;
           
            gd.offered_id = offered_id;
            gd.user_id = user_id;
            gd.datetime = now;
            gd.discussion_title = title;
            gd.status = status;


            try
            {
                dcis.Topics.Add(gd);
                dcis.SaveChanges();
                ViewBag.Result3 = "Saved";
            }
            catch (Exception e)
            {
                ViewBag.Result3 = "NOPE";
                throw e;

            }

            return RedirectToAction("Topics", new { offered_course_id = offered_id });
        }
    }
}