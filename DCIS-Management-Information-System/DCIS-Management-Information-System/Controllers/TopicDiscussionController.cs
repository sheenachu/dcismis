﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using DCIS_Management_Information_System.Models;

namespace DCIS_Management_Information_System.Controllers
{
    public class TopicDiscussionController : Controller
    {
        // GET: TopicDiscussion
        public ActionResult Index()
        {
            int topic_id = Convert.ToInt32(Request.QueryString["TopicID"]);
            int courseID = Convert.ToInt32(Request.QueryString["OfferedCourseID"]);

            DCIS_DBEntities db = new DCIS_DBEntities();

            var Result = (from u in db.Topic_Discussion_Entry
                          where u.topic_id == topic_id
                          select u).OrderByDescending(u => u.createdAt);

            var Result2 = (from u in db.Topic_Discussion_Reply
                           select u);

            var TopicTitle = (from u in db.Topics
                              where u.topic_id == topic_id
                              select u);

            var CourseDetails = (from c in db.Courses
                                 join o in db.Offereds on c.course_id equals o.course_id
                                 where o.offered_id == courseID
                                 select new CourseDetails
                                 {
                                     code = c.code,
                                     description = c.title
                                 });

            //join to Offered and Course to get Course.description and Course.code
            //fetch User.fname, User.mname, User.lname

            ViewData["ResultSet"] = Result.ToList();
            ViewData["Replies"] = Result2.ToList();
            ViewData["CourseDetails"] = CourseDetails.ToList();
            ViewData["TopicTitle"] = TopicTitle.ToList();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult SaveInDatabase(HttpPostedFileBase file, int topic_id, string post)
        {
            DateTime now = DateTime.Now;
            string format = "h:mm tt";
            var discussion_id = 0;
            var filename = "";
            var name = "";
            var ext = "";

            string csvData = "";

            if (Request.Files.Count > 0)
            {
                Random r = new Random();
                Byte[] values = new Byte[1];
                r.NextBytes(values);

                filename = Path.GetFileName(file.FileName);
                ext = Path.GetExtension(file.FileName);
                name = Path.GetFileNameWithoutExtension(file.FileName);

                filename = name + "_" + DateTime.Today.ToString("yymmdd") + "_" + values[0] + ext;

                var path = Path.Combine(Server.MapPath("~/Assets/UploadedFiles"), filename);
                file.SaveAs(path);


                csvData = System.IO.File.ReadAllText(path);
            }

            try
            {
                var user_id = 2;
                var datetime = now.ToString(format);

                Topic_Discussion_Entry save = new Topic_Discussion_Entry();
                DCIS_DBEntities db = new DCIS_DBEntities();

                save.user_id = user_id;
                save.topic_discussion_post = post;
                save.time = datetime;
                save.createdAt = now;
                save.topic_id = topic_id;

                if (Request.Files.Count > 0)
                {
                    save.filename = filename;
                }

                db.Topic_Discussion_Entry.Add(save);
                db.SaveChanges();

                discussion_id = db.Topic_Discussion_Entry.Max(p => p.topic_discussion_entry_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            var ret = new { discussion_id = discussion_id, filename = name + ext, full_file_name = filename, ext = ext, csvData = csvData };

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveReplyInDatabase(Topic_Discussion_Reply reply)
        {
            DateTime now = DateTime.Now;
            string format = "h:mm tt";

            try
            {
                var user_id = 2;
                var datetime = now.ToString(format);

                Topic_Discussion_Reply save = new Topic_Discussion_Reply();
                DCIS_DBEntities db = new DCIS_DBEntities();

                save.topic_discussion_entry_id = reply.topic_discussion_entry_id;
                save.user_id = user_id;
                save.time = datetime;
                save.createdAt = now;
                save.topic_discussion_reply_post = reply.topic_discussion_reply_post;

                db.Topic_Discussion_Reply.Add(save);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(reply.topic_discussion_reply_post, JsonRequestBehavior.AllowGet);
        }

        public FileResult Download(string filename)
        {
            string path = Server.MapPath("~/Assets/UploadedFiles");

            string fullpath = Path.Combine(path, filename);
            return File(fullpath, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }
    }
}