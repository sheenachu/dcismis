﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DCIS_Management_Information_System.Models;

namespace DCIS_Management_Information_System.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Dashboard()//Teacher View
        {
            int school_id = 12312312;
            DCIS_DBEntities de = new DCIS_DBEntities();
            var list = (from a in de.Teachers
                        join b in de.Offereds on a.offered_id equals b.offered_id
                        join c in de.Courses on b.course_id equals c.course_id where a.school_id == school_id
                        select new ScheduleModel
                        {
                           offered_id = b.offered_id,
                           code = c.code,
                           description = c.title,
                           start_time = b.start_time,
                           end_time = b.end_time,
                           day = b.day
                           
                        }).ToList();
            ViewData["data"] = list;
          
            return View(list);
        }
        public ActionResult DashboardStudent()
        {
            var user_id = 2;
            //var query = (from)
            DCIS_DBEntities de = new DCIS_DBEntities();
            var list = (from a in de.Users
                        join b in de.Students_in_offered_courses on a.schoolId equals b.school_id
                        join c in de.Offereds on b.offered_id equals c.offered_id
                        join d in de.Courses on c.course_id equals d.course_id
                        where b.school_id == user_id
                        select new SchedStud
                        {
                            offered_id = c.offered_id,
                            first_name = a.fname,
                            last_name = a.lname,
                            start_time = c.start_time,
                            end_time = c.end_time,
                            day = c.day,
                            code = d.code,
                            description = d.title,
                            request_status = b.request_status
                        }).ToList();
            ViewData["data"] = list;

            return View(list);
        }
        /*[HttpGet]
        public ActionResult SearchPartialView()
        {
            DCISMISEntities1 de = new DCISMISEntities1();

            var list = (from a in de.Users
                        join b in de.Teachers on a.user_id equals b.user_id
                        join c in de.Offereds on b.offered_id equals c.offered_id
                        join d in de.Courses on c.course_id equals d.course_id
                        select new SchedStud
                        {
                            first_name = a.first_name,
                            last_name = a.last_name,
                            start_time = c.start_time,
                            end_time = c.end_time,
                            day = c.day,
                            code = d.code,
                            description = d.description
                        });
            ViewBag.data = list;


            return PartialView(list);
        }*/
        public ActionResult SearchSchedule()
        {
            var user_id = 2;
            //var query = (from)
            DCIS_DBEntities de = new DCIS_DBEntities();
            var list = (from a in de.Offereds
                        join b in de.Courses on a.course_id equals b.course_id
                        join g in de.Rooms on a.room_id equals g.room_id
                        where !(from c in de.Students_in_offered_courses where c.school_id == user_id select c.offered_id).Contains(a.offered_id)
                        select new SchedStud
                        {
                            offered_id = a.offered_id,
                            code = b.code,
                            description = b.title,
                            day = a.day,
                            start_time = a.start_time,
                            end_time = a.end_time,
                            room_name = g.name

                        }).ToList();

            ViewData["data"] = list;
            return View(list);
        }
        //public ActionResult UpdateStatus()
        //{
        //    var user_id = 2;
        //    var offered_id = Convert.ToInt32(Request.QueryString["offered_id"]);
        //    DCISMISEntities1 de = new DCISMISEntities1();
        //    var stud = de.Students.SingleOrDefault(a => a.offered_id == offered_id && a.user_id==user_id);
        //    stud.request_status = "pending";

        //    de.SaveChanges();
        //    //var query = (from)

        //    return RedirectToAction("DashboardStudent", "Home");
        //}

        public ActionResult EnrollStudent()
        {
            var user_id = 2;
            var offered_id = Convert.ToInt32(Request.QueryString["offered_id"]);
            DCIS_DBEntities de = new DCIS_DBEntities();
            Students_in_offered_courses u = new Students_in_offered_courses();
            u.school_id = user_id;
            u.offered_id = offered_id;
            u.request_status = "enrolled";

            try
            {
                de.Students_in_offered_courses.Add(u);
                de.SaveChanges();
            }
            catch
            {
                ViewBag.Result = "Error";
            }


            //var query = (from)

            return RedirectToAction("SearchSchedule", "Home");
        }
        public ActionResult Delete()
        {
            var user_id = 2;
            DCIS_DBEntities fe = new DCIS_DBEntities();
            int offered_id = Convert.ToInt32(Request.QueryString["offered_id"]);
            var c = (from p in fe.Students_in_offered_courses
                     where p.offered_id == offered_id && p.school_id == user_id
                     select p).FirstOrDefault();
            fe.Students_in_offered_courses.Remove(c);
            fe.SaveChanges();
            return RedirectToAction("DashboardStudent", "Home");
            //return View();
        }



    }

}
