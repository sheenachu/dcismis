﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using DCIS_Management_Information_System.Models;

namespace DCIS_Management_Information_System.Controllers
{
    public class StudentInvitationController : Controller
    {
        // GET: StudentInvitation
        public ActionResult GroupListStudent()
        {
            //NEED SESSION ID
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Group g = new Group();
            var groupList = dcis.Groups.SqlQuery("SELECT * FROM [Group]");
            ViewData["group"] = groupList.ToList();
            return View();
           
        }

        public ActionResult UserList()
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            var detailList = dcis.Users.SqlQuery("SELECT * FROM [User] WHERE schoolId NOT IN (SELECT school_id FROM Users_In_Group)");
            ViewData["ListOfUsers"] = detailList.ToList();
            return View();
        }
        
        public ActionResult GroupList()
        {
            //NEED SESSION ID
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            Group g = new Group();
            var groupList = dcis.Groups.SqlQuery("SELECT * FROM [Group]");
            ViewData["group"] = groupList.ToList();
            return View();
        }

        public ActionResult DisplayInvitation()
        {
        

            return RedirectToAction("Topics", "Discussion");
            return View("Topics");
        }

        //public ActionResult DisplayInvitationStudent()
        //{
        //    DCIS_DBEntities dcis = new DCIS_DBEntities();
        //    User user = new User();
        //    Student sad = new Student();
        //    var pendingUser = dcis.Users.SqlQuery("SELECT * FROM [User] u Left Join student s on s.[user_id] = u.[user_id] WHERE request_status = 'pending'");
        //    var acceptedUserStudent = dcis.Users.SqlQuery("SELECT * FROM [User] u Left Join student s on s.[user_id] = u.[user_id] WHERE request_status = 'accepted' AND u.role = 'student'");
        //    var invitedTeacher = dcis.Users.SqlQuery("SELECT * FROM [User] u Left Join student s on s.[user_id] = u.[user_id] WHERE request_status = 'accepted' AND (u.role = 'teacher' OR u.role = 'chairman')");

        //    ViewData["pendingUser"] = pendingUser.ToList();
        //    ViewData["acceptedStudent"] = acceptedUserStudent.ToList();
        //    ViewData["invitedTeacher"] = invitedTeacher.ToList();

        //    return View();
        //}

        public JsonResult GetSearchValue(string search)
        {
            DCIS_DBEntities db = new DCIS_DBEntities();
            List<UserModel> allsearch = db.Users.Where(x => x.fname.Contains(search)).Select(x => new UserModel
            {
                User_id = x.schoolId,
                First_name = x.fname,
                Middle_name = x.mname,
                Last_name = x.lname,
                Role = x.userType
            }).ToList();
            return new JsonResult { Data = allsearch, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult AcceptPerson()
        {
            var UserId = Convert.ToInt32(Request.QueryString["UserId"]);
            DCIS_DBEntities pe = new DCIS_DBEntities();
            var p = pe.Users_In_Group.SingleOrDefault(b => b.school_id == UserId);
            p.request_status = "accepted";


            pe.SaveChanges();

            return RedirectToAction("DisplayInvitation");
        }

        public ActionResult DeclinePerson()
        {
            var UserId = Convert.ToInt32(Request.QueryString["UserId"]);


            DCIS_DBEntities pe = new DCIS_DBEntities();
            var p = pe.Users_In_Group.SingleOrDefault(b => b.school_id == UserId);
            p.request_status = "declined";


            pe.SaveChanges();

            return RedirectToAction("DisplayInvitation");
        }

        public ActionResult AddUser()
        {
            var uid = Convert.ToInt32(Request.QueryString["UserId"]);

            int groupId = 5;

            DCIS_DBEntities pe = new DCIS_DBEntities();
            Users_In_Group p = new Users_In_Group();
            p.school_id = uid;
            p.group_id = groupId;
            p.request_status = "accepted";
            try
            {
                pe.Users_In_Group.Add(p);
                pe.SaveChanges();
            }
            catch (Exception e)
            {
                ViewBag.Result = "Error";
            }
            return View("StudentInvitation");
        }

        public ActionResult AddGroup(FormCollection fc)
        {
            var groupname = fc["groupname"].ToString();
            var description = fc["description"].ToString();
            int creatorId = 2;
            int groupId = 0;


            DCIS_DBEntities pe = new DCIS_DBEntities();
            Group p = new Group();
            Users_In_Group ug = new Users_In_Group();

            //Creation of Group
            p.school_id = creatorId;
            p.group_name = groupname;
            p.description = description;
            try
            {
                pe.Groups.Add(p);
                pe.SaveChanges();
                groupId = pe.Groups.Max(grp => grp.group_id);
            }
            catch (Exception e)
            {
                ViewBag.Result = "Error";
            }

            //Insertion of Creator to the Users_In_Group Table (1st Memnber is the Creator)
            ug.group_id = groupId;
            ug.school_id = creatorId;
            ug.request_status = "creator";
            try
            {
                pe.Users_In_Group.Add(ug);
                pe.SaveChanges();
            }
            catch (Exception e)
            {
                ViewBag.Result = "Error";
            }

            return RedirectToAction("GroupList");

        }

        public ActionResult AddStudent()
        {
            int group_id = Convert.ToInt32(Request.QueryString["GroupId"]);
            var UserId = Convert.ToInt32(Request.QueryString["UserId"]);


            DCIS_DBEntities pe = new DCIS_DBEntities();
            Users_In_Group users = new Users_In_Group();

            var p = pe.Users_In_Group.SingleOrDefault(b => b.school_id == UserId);
            users.request_status = "accepted";
            users.group_id = group_id;
            users.school_id = UserId;

            try
            {
                pe.Users_In_Group.Add(users);
                pe.SaveChanges();
            }
            catch (Exception e)
            {
                ViewBag.Result = "Error";
            }
            return RedirectToAction("GroupDiscussion", new { GroupId = group_id});
        }

        public ActionResult DeleteStudent()
        {
            DCIS_DBEntities fe = new DCIS_DBEntities();
            int userID = Convert.ToInt32(Request.QueryString["userID"]);
            var c = (from p in fe.Users_In_Group
                     where p.school_id == userID
                     select p).FirstOrDefault();


            fe.Users_In_Group.Remove(c);
            fe.SaveChanges();
            return RedirectToAction("DisplayInvitation");
        }
        
        public ActionResult GroupDiscussion()
        {
            DCIS_DBEntities dcis = new DCIS_DBEntities();
            int group_id = Convert.ToInt32(Request.QueryString["GroupId"]);
            var membersList = (from u in dcis.Users
                               join ug in dcis.Users_In_Group on u.schoolId equals ug.school_id
                               where ug.group_id == group_id && ug.request_status != "creator"
                               select new GroupMembers
                               {
                                   schoolId = u.schoolId,
                                   fname = u.fname,
                                   lname = u.lname,
                                   mname = u.mname,
                                   userType = u.userType

                               }).ToList();

            var groupCreator = (from u in dcis.Users
                               join ug in dcis.Users_In_Group on u.schoolId equals ug.school_id
                               where ug.group_id == group_id && ug.request_status == "creator"
                               select new GroupMembers
                               {
                                   schoolId = u.schoolId,
                                   fname = u.fname,
                                   lname = u.lname,
                                   mname = u.mname,
                                   userType = u.userType

                               }).ToList();

            ViewData["GroupMembers"] = membersList.ToList();
            ViewData["GroupCreator"] = groupCreator.ToList();

            //BEGIN JAN//
            DCIS_DBEntities db = new DCIS_DBEntities();

            var GroupDetails = (from g in db.Groups
                                where g.group_id == group_id
                                select g);

            var Entries = (from g in db.Group_Discussion_Entry
                           where g.group_id == group_id
                           select g).OrderByDescending(g => g.createdAt);

            var Replies = (from g in db.Group_Discussion_Reply
                           select g);

            ViewData["GroupDetails"] = GroupDetails.ToList();
            ViewData["Entries"] = Entries.ToList();
            ViewData["Replies"] = Replies.ToList();
            //END JAN
            return View();
        }

        //START JAN//
        [HttpPost]
        [AllowAnonymous]
        public JsonResult SaveInDatabase(HttpPostedFileBase file, int topic_id, string post)
        {
            DateTime now = DateTime.Now;
            string format = "h:mm tt";
            var discussion_id = 0;
            var filename = "";
            var name = "";
            var ext = "";

            var group_id = Convert.ToInt32(Request.QueryString["GroupID"]);

            if (Request.Files.Count > 0)
            {
                Random r = new Random();
                Byte[] values = new Byte[1];
                r.NextBytes(values);

                filename = Path.GetFileName(file.FileName);
                ext = Path.GetExtension(file.FileName);
                name = Path.GetFileNameWithoutExtension(file.FileName);

                filename = name + "_" + DateTime.Today.ToString("yymmdd") + "_" + values[0] + ext;

                var path = Path.Combine(Server.MapPath("~/Assets/UploadedFiles"), filename);
                file.SaveAs(path);
            }

            try
            {
                var user_id = 2;
                var datetime = now.ToString(format);

                Group_Discussion_Entry save = new Group_Discussion_Entry();
                DCIS_DBEntities db = new DCIS_DBEntities();

                save.group_id = topic_id;
                save.school_id = user_id;
                save.time = datetime;
                save.post = post;
                save.createdAt = now;
                //save.group_discussion_entry_id = topic_id;

                if (Request.Files.Count > 0)
                {
                    save.filename = filename;
                }

                db.Group_Discussion_Entry.Add(save);
                db.SaveChanges();

                discussion_id = db.Group_Discussion_Entry.Max(p => p.group_discussion_entry_id);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            var ret = new { discussion_id = discussion_id, filename = name + ext, full_file_name = filename, ext = ext };

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveReplyInDatabase(Topic_Discussion_Reply reply)
        {
            DateTime now = DateTime.Now;
            string format = "h:mm tt";

            try
            {
                var user_id = 2;
                var datetime = now.ToString(format);

                Group_Discussion_Reply save = new Group_Discussion_Reply();
                DCIS_DBEntities db = new DCIS_DBEntities();

                save.group_discussion_entry_id = reply.topic_discussion_entry_id;
                save.school_id = user_id;
                save.time = datetime;
                save.createdAt = now;
                save.post = reply.topic_discussion_reply_post;

                db.Group_Discussion_Reply.Add(save);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(reply.topic_discussion_reply_post, JsonRequestBehavior.AllowGet);
        }

        public FileResult Download(string filename)
        {
            string path = Server.MapPath("~/Assets/UploadedFiles");

            string fullpath = Path.Combine(path, filename);
            return File(fullpath, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }

        //END JAN//

        public ActionResult Group()
        {
            int group_id = Convert.ToInt32(Request.QueryString["GroupID"]);
            //BEGIN JAN//
            DCIS_DBEntities db = new DCIS_DBEntities();

            var GroupDetails = (from g in db.Groups
                                where g.group_id == group_id
                                select g);

            var Entries = (from g in db.Group_Discussion_Entry
                           where g.group_id == group_id
                           select g).OrderByDescending(g => g.createdAt);

            var Replies = (from g in db.Group_Discussion_Reply
                           select g);

            ViewData["GroupDetails"] = GroupDetails.ToList();
            ViewData["Entries"] = Entries.ToList();
            ViewData["Replies"] = Replies.ToList();
            //END JAN
            return View();
        }
    }
}