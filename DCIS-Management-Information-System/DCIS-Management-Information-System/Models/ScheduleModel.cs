﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCIS_Management_Information_System.Models
{
    public class ScheduleModel
    {
        public int offered_id { get; set; }
        public int start_time { get; set; }
        public int end_time { get; set; }
        public string day { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }
}