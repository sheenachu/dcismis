﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCIS_Management_Information_System.Models
{
    public class Discussion
    {
        public int offered_course_discussion_id { get; set; }
        public int offered_id { get; set; }
        public int user_id { get; set; }
        public System.DateTime datetime { get; set; }
        public string discussion_title { get; set; }
        public int status { get; set; }
    }
}