﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCIS_Management_Information_System.Models
{
    public class SchedStud
    {
        public int user_id { get; set; }
        public int offered_id { get; set; }
        public string room_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int start_time { get; set; }
        public int end_time { get; set; }
        public string day { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string request_status { get; set; }

    }
}