﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCIS_Management_Information_System.Models
{
    public class CourseDetails
    {
        public string code { get; set; }
        public string description { get; set; }
    }
}