﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCIS_Management_Information_System.Models
{
    public class GroupMembers
    {
        public int schoolId { get; set; }
        public string userType { get; set; }
        public string fname { get; set; }
        public string mname { get; set; }
        public string lname { get; set; }
    }
}