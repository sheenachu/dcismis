//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DCIS_Management_Information_System
{
    using System;
    using System.Collections.Generic;
    
    public partial class Online_Sources
    {
        public int onlineSource_ID { get; set; }
        public int course_FK { get; set; }
        public string webpageName { get; set; }
        public string weblink { get; set; }
        public Nullable<int> syllabus_FK { get; set; }
    
        public virtual Syllabu Syllabu { get; set; }
    }
}
