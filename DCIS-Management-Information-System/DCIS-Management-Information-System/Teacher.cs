//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DCIS_Management_Information_System
{
    using System;
    using System.Collections.Generic;
    
    public partial class Teacher
    {
        public int teacher_id { get; set; }
        public int offered_id { get; set; }
        public int school_id { get; set; }
        public Nullable<int> hours { get; set; }
    
        public virtual Offered Offered { get; set; }
        public virtual User User { get; set; }
    }
}
