//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DCIS_Management_Information_System
{
    using System;
    using System.Collections.Generic;
    
    public partial class People_In_Group
    {
        public int people_in_group_id { get; set; }
        public int group_discussion_id { get; set; }
        public int user_id { get; set; }
        public int group_id { get; set; }
    
        public virtual User User { get; set; }
        public virtual Group Group { get; set; }
        public virtual Group_Discussion Group_Discussion { get; set; }
    }
}
